from __future__ import annotations
from typing import List
from flask_sqlalchemy import SQLAlchemy

db: SQLAlchemy = SQLAlchemy()

class Team(db.Model):
    __tablename__ = "team"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)

    latitude = db.Column(db.Float, nullable=True)
    longitude = db.Column(db.Float, nullable=True)

    team_captain_id = db.Column(db.Integer, nullable=False)
    team_members: db.Mapped[List["User"]] = db.relationship(back_populates="team")

class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.Text, nullable=False)
    last_name = db.Column(db.Text, nullable=False)

    team_id: db.Mapped[int] = db.mapped_column(db.ForeignKey("team.id"), nullable=True)
    team: db.Mapped["Team"] = db.relationship(back_populates="team_members")

class Teacher(db.Model):
    __tablename__ = "teacher"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.Text, nullable=False)
    last_name = db.Column(db.Text, nullable=False)
    email = db.Column(db.Text, unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)

    latitude = db.Column(db.Float, nullable=True)
    longitude = db.Column(db.Float, nullable=True)