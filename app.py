from flask import Flask, render_template, request, flash, session, url_for, redirect
from postgresqlite import get_uri
from models import db, Team, User, Teacher
import folium
import os, shutil
from geopy.distance import geodesic

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = get_uri()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'secret key'

db.init_app(app)

# Run using `poetry install && poetry run flask run --reload`
@app.route('/', methods=["GET"])
def index():
    # Check if user is logged in
    if not 'user_id' in session.keys():
        return redirect("/login")

    return render_template('index.html')

@app.route('/location',methods=["POST"])
def location():
    if not 'user_id' in session.keys():
        return redirect("/login")

    user_id = session['user_id']

    json = request.get_json()


    team = Team.query.filter_by(team_captain_id=int(user_id[1:])).first()

    if team:
        team.longitude = json['longitude']
        team.latitude = json['latitude']

    db.session.commit()
    return 
    #TypeError: The view function for 'location' did not return a valid response. 
    #The function either returned None or ended without a return statement.

@app.route('/login', methods=["GET"])
def login_page():
    return render_template('login.html')

@app.route('/login', methods=["POST"])
def login():
    first_name: str = request.form.get('first_name', None)
    last_name: str = request.form.get('last_name', None)
    password: str = request.form.get('password', None)
    # Check form data
    if not first_name or not last_name:
        print("Error: input fields are empty")
        return render_template("login.html")

    if password:
        teacher = Teacher.query.filter_by(first_name=first_name, last_name=last_name, password=password).first()
        if not teacher:
            # Create new teacher
            teacher = Teacher(first_name=first_name.lower(), last_name=last_name.lower(), password=password)
            db.session.add(teacher)
        session['user_id'] = f't{teacher.id}'
    else:
        # Look up existing user
        user = User.query.filter_by(first_name=first_name, last_name=last_name).first()
        db.session.flush()

        # Check if user does not exist
        if not user:
            # Create new user
            user = User(first_name=first_name.lower(), last_name=last_name.lower())
            db.session.add(user)
        session['user_id'] = f's{user.id}'

    db.session.commit()

        # Create session with user id


    return render_template('index.html')

@app.route('/logout', methods=["GET"])
def logout():
    # Check if user is logged in
    if 'user_id' in session.keys():
        del session['user_id']
    
    if 'team_id' in session.keys():
        del session['team_id']

    return redirect("/login")

@app.route('/create_team', methods=["GET"])
def create_team_form():
    if not 'user_id' in session.keys():
        return redirect("/login")

    user_id = session['user_id']

    team_members = User.query.filter_by(team_id=None).filter(User.id != int(user_id[1:])).all()

    return render_template('create_team.html', team_members=team_members)

@app.route('/create_team', methods=["POST"])
def create_team():
    if not 'user_id' in session.keys():
        return redirect("/login")
    
    user_id = session['user_id']

    # Get form data
    team_name = request.form.get('team_name')
    team_members = request.form.getlist('team_members') #list of user id's

    # Create team
    team = Team(name=team_name, team_captain_id=int(user_id[1:]))
    db.session.add(team)
    db.session.flush()
    
    # Update team captain with team id
    db.session.execute(
        db.update(User)
        .where(User.id == int(user_id[1:]))
        .values(team_id=team.id)
    )
    db.session.flush()

    # Update all other users with team id
    for member in team_members:
        db.session.execute(
            db.update(User)
            .where(User.id == int(member))
            .values(team_id=team.id)
        )
    
    db.session.commit()

    return redirect('/')

def calculate_distance(point_a: (float, float), point_b: (float,float)) -> int:
    return round(geodesic(point_a, point_b).meters)

@app.route('/map')
def create_map():
    if not 'user_id' in session.keys(): #maybe just in session
        return redirect("/login")
    
    user_id = session['user_id']
    teacher = None
    student = None
    if user_id.startswith('s'):
        student_id = int(user_id[1:])
        student = User.query.filter_by(id=student_id).first()
    else:
        teacher = Teacher.query.filter_by(id=int(user_id[1:])).first()


    # Create a map centered on a specific location
    folium_map = folium.Map(location=[52.22015864142798, 6.895649366355526], zoom_start=17)

    # Add a marker to the map
    #folium.Marker(location=[52.2215, 6.8937], popup='Team B').add_to(folium_map)
    #for loop markers

    distance_to_teachers = {}
    if teacher:
        points = Team.query.all()
    else: 
        points = [Team.query.filter_by(id=student.team_id).first()]

        for teacher in Teacher.query.all():
            distance_to_teachers[teacher.first_name] = calculate_distance((points[0].latitude, points[0].longitude),(teacher.latitude, teacher.longitude))


    for point in points:
        folium.Marker(location=[point.latitude, point.longitude], popup=point.name).add_to(folium_map)

    # Save the map to an HTML file
    folium_map.save('static/map.html')
    

    # distance_to_teachers = {
    #     'Tibor': 300,
    #     'Frank': 250,
    #     'Ivo': 600,
    #     'Sander': 10,
    #     'Timothy': 80,
    #     'Remco': 90
    # }
    
    return render_template('map_embedded.html',distance_to_teachers=distance_to_teachers)

if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()

        # Create mock data
        # Users
        users = [{"first_name":"tom", "last_name":"jan"}, {"first_name":"piet", "last_name":"sanders"}]
        for u in users:
            user = User(first_name=u['first_name'], last_name=u['last_name'])
            db.session.add(user)
            db.session.flush()
            
        # Team
        user = User(first_name="Henk", last_name="Bol")
        db.session.add(user)
        db.session.flush()
        team = Team(name='Het Team', team_captain_id=user.id, latitude=52.22042446786115, longitude=6.893495746383436)
        db.session.add(team)
        db.session.flush()
        db.session.execute(
            db.update(User)
            .where(User.id == user.id)
            .values(team_id=team.id)
        )
        
        teacher = Teacher(first_name='Tibor', last_name='Derksen',email='example@gmail.com', password='2',latitude=52.220509724788734, longitude=6.897545520733569)
        db.session.add(teacher)
        
        db.session.commit()

app.run()